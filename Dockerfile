FROM ubuntu:22.10

# Installing build dependencies and tools
RUN apt-get update && apt-get install -y git build-essential libsdl2-dev bison libpng-dev bsdmainutils
RUN git clone 'https://github.com/gbdev/rgbds.git' /rgbds
RUN cd /rgbds && make && make install && rm -rf /rgbds

# Building linux native, libretro and bootroms targets
WORKDIR /sameboy
ENV CONF=release
CMD ["/usr/bin/bash", "-c", "git clone 'https://github.com/LIJI32/SameBoy.git' /sameboy && make bootroms sdl libretro && mv /sameboy/build/* /app"] 
